FROM alpine
RUN apk --no-cache add libarchive-tools curl
ARG nexmarkurl=http://odysseus.informatik.uni-oldenburg.de/download/nexmark/nexmark.linux.gtk.x86_64.zip
RUN curl ${nexmarkurl} | bsdtar -xvf- -C /usr/lib/ && chmod +x /usr/lib/nexmark/nexmark

FROM openjdk:8-jre
EXPOSE 65440 65441 65442 65443
COPY --from=0 /usr/lib/nexmark /usr/lib/nexmark
ENTRYPOINT ["/usr/lib/nexmark/nexmark"]
